#!/bin/bash

root_path=$PWD
repo=$(git branch --show-current)
echo "Current branch: $repo"

write_metadata(){
   echo "{
        	\"artefact\":
                [{
                	\"name\":\"${root_path}/build/libs/*.war\",
                	\"destination\":\"TOMCAT_BACK\"
                }]
        }" >> "${root_path}"/build/metadata.json
}

set -eE

bop_server_rule=${root_path}/bopserver/electronic-bop-form/rules/reg_rule_repo/
finsurv_rules=${root_path}/bopserver/electronic-bop-form/rules
dist_lib_js=${root_path}/bopserver/dist/js/lib.js
dist_main_js=${root_path}/bopserver/dist/js/main.js

git submodule update --init --recursive

if [ -d "$bop_server_rule" ] && [ -d "$finsurv_rules" ]; then

  # Build the submodules...
  echo "Building bopserver..."
  cd bopserver
  git checkout -- package-lock.json
  git checkout -- package.json
  git checkout "$repo"
  git pull

  echo "Building electronic-bop-form..."
  cd electronic-bop-form
  git checkout -- package-lock.json
  git checkout -- package.json
  git checkout "$repo"
  git pull

  echo "Building rules..."
  cd rules
  git checkout -- package-lock.json
  git checkout -- package.json
  git checkout "$repo"
  git pull
  npm install

  cd ../../

  npm install
  npm run build

  cd ../

  # Now check if the built artefacts exist...
  if [ -f "$dist_lib_js" ] && [ -f "$dist_main_js" ]; then

    # Build rules-management
    ./gradlew clean
    ./gradlew processResources
    ./gradlew build --refresh-dependencies

    if [ $? -eq 0 ]; then
      write_metadata
    else
      echo "Building WAR files failed"
      exit 1
    fi

	else
    if [ -f "$dist_lib_js" ]; then
      echo "$dist_lib_js is missing"
    fi

    if [ -f "$dist_main_js" ]; then
      echo "$dist_main_js is missing"
    fi

    exit 1
	fi

else
  if [ -d "$bop_server_rule" ]; then
    echo "$bop_server_rule is missing"
  fi

  if [ -d "$finsurv_rules" ]; then
    echo "$finsurv_rules is missing"
  fi

	exit 1
fi