#!/bin/bash

echo "============================================"
echo "GENERAL RULES MANAGER BUILD..."
echo "============================================"

function checkoutRepo(){
  sourceDir=$1
  repoName=$2
  repoUrl=$3
  if [ ! -d "$sourceDir/$repoName" ]
  then
    echo "Clone $repoName repo..."
    git clone "$repoUrl"
    if [ $? -eq 0 ]; then
      echo "Git Clone successful."
    else
      exit 1;
    fi
  fi
}

function incrementBuildVersion(){
	echo "...Incrementing build version..."
	previousVersionStr=$(grep -Po '(?<=\"[Vv][Ee][Rr][Ss][Ii][Oo][Nn]\":\")([^"]*)' ./version.json)
	./bump_version.sh build
	currentVersionStr=$(grep -Po '(?<=\"[Vv][Ee][Rr][Ss][Ii][Oo][Nn]\":\")([^"]*)' ./version.json)
	echo "latest tag to use: $currentVersionStr"
}

function tagVersion(){
	echo "Adding new version tag: $currentVersionStr..."
	git tag -a "$versionPrefixStr$currentVersionStr" -m "[BUILD][RULESMANAGEMENT] $versionPrefixStr$currentVersionStr"
	current_branch=$(git symbolic-ref --short HEAD)
	echo "Pushing branch $current_branch and tag $currentVersionStr upstream..."
	git push origin "$versionPrefixStr$currentVersionStr" --tags
}

function getDocumentation(){
	repoName=$1
	docsFile="$destinationFolder/CHANGELOG.TXT"
	#NB: for details around formatting the log lines see the "PRETTY FORMATS" section in the git-log documentation (file:///C:/Program%20Files/Git/mingw64/share/doc/git-doc/git-log.html).
	#gitTags=$(git describe --tags --abbrev=0)
	#gitTags="$versionPrefixStr$previousVersionStr"
	gitTags=$(git tag --list -i | grep "^$versionPrefixStr.*" | tail -n 1)
	logOutput=""
	fileOutput=""
	#git log $(git describe --tags --abbrev=0)..HEAD --pretty=format:"%s" -i -E --grep="^(\[INTERNAL\]|\[FEATURE\]|\[FIX\]|\[DOC\])*\[FEATURE\]"
	echo "  *****  GIT TAG :: $gitTags  *****  "
	echo "$repoName:" >> $docsFile
	echo "------------------------------" >> $docsFile
	if [[ "$gitTags" != "" ]]; then
		echo "< PreviousTag: $gitTags >" >> $docsFile
		echo "------------------------------"
		echo " $repoName / Delta ($gitTags) -> HEAD "
		echo "------------------------------"
		logOutput=$(git log $gitTags..HEAD --pretty=format:"%ai %an <%ae>: %s")
		echo "Changelog:"
		echo "$logOutput"
		echo ""
		fileOutput=$(git diff --name-status $gitTags HEAD)
		echo "Affected Files:"
		echo "$fileOutput"
		echo ""
		echo "------------------------------"
	else 
		echo "< No previous tag detected >" >> $docsFile
		logOutput=$(git log --pretty=format:"%ai %an <%ae>: %s" --since=1.months | head -n 15)
	fi
	if [[ $logOutput ]]; then
		echo "$logOutput" >> $docsFile
	else 
		echo "...no changes to document." >> $docsFile
	fi
	if [[ $fileOutput ]]; then
		echo "Affected Files:" >> $docsFile
		echo "$fileOutput" >> $docsFile
	fi
	echo " " >> $docsFile
	echo " " >> $docsFile
	echo " " >> $docsFile
}

function primeDocumentation(){
	buildInfo=$1
	docsFile="$destinationFolder/CHANGELOG.TXT"
	echo " " >> $docsFile
	echo "========================================" >> $docsFile
	echo " $buildInfo" >> $docsFile
	echo "========================================" >> $docsFile
	echo "----------------------------------------" >> $docsFile
	echo " < Change log file: $docsFile >" >> $docsFile
	echo "----------------------------------------" >> $docsFile
	echo " " >> $docsFile
	echo " " >> $docsFile
}

function closeDocumentation(){
	docsFile="$destinationFolder/CHANGELOG.TXT"
	consolodatedDocsFile="$rootFolder/dist/CONSOLODATED_CHANGELOG.TXT"
	curDate=$(date +"%Y-%m-%d %T")
	#git log $(git describe --tags --abbrev=0)..HEAD --pretty=format:"%s" -i -E --grep="^(\[INTERNAL\]|\[FEATURE\]|\[FIX\]|\[DOC\])*\[FEATURE\]"
	echo " " >> $docsFile
	echo " " >> $docsFile
	echo "########################################################################################################################" >> $docsFile
	echo " " >> $docsFile
	echo " BUILD ($versionPrefixStr$currentVersionStr  @  $curDate) COMPLETE. " >> $docsFile
	echo " < Change log file: $docsFile >" >> $docsFile
	echo " " >> $docsFile
	echo "########################################################################################################################" >> $docsFile
	echo " " >> $docsFile
	echo " " >> $docsFile
	echo " " >> $docsFile
	cat "$docsFile" >> "$consolodatedDocsFile"
}

workingDir=$(pwd)
versionPrefixStr="RM_v"
currentVersionStr=""
previousVersionStr=""
incrementBuildVersion
#echo "...Working dir (return point): $workingDir"
runSilent=false
 
while [ $# -gt 0 ] ; do
  case $1 in
    -r | -root) rootFolder="$2" ;;
    -d | -dest) destinationFolder="$2" ;;
    -s | -silent) runSilent=true ;;
  esac
  shift
done

printf "\n----------------------------"
printf "\nReceived Parameters"
printf "\n----------------------------\n"
printf "Root Folder: "$rootFolder"\nDestination Folder: "$destinationFolder"\nRun Silent: "$runSilent
printf "\n----------------------------\n"

#-----  SCRIPT USAGE  -----#
# echo -e "\nUsage: $0 --root [root-build-dir] -- dest [optional:dest-deployment-dir] --silent"; 
[ -z "$rootFolder" ] && { 
	echo -e "\n-----------------------------------------------------------------------"
	echo -e "Script Usage"
	echo -e "-----------------------------------------------------------------------"
	echo -e "Mandatory parameters:"
	echo -e "-root [param] -> the root build directory"
	echo -e "\nOptional parameters:"
	echo -e "-dest [param] -> the root build directory"
	echo -e "-silent -> use default settings and skip user input"
	echo -e "\nExample:"
	echo "$0 -root /d/root -dest /d/destination -silent"; 
	echo -e "-----------------------------------------------------------------------"
	exit 1;
}

echo "...Root build directory: $rootFolder"
#remove trailing "/" from root path...
re="^(.+)\/$"
if [[ $rootFolder =~ $re ]]; then
  echo "...remove trailing slash '/'..."
  rootFolder="${BASH_REMATCH[1]}";
fi
rootFolder="$rootFolder"
sourceFolder="$rootFolder/src"

#-----  SOURCE FOLDER  -----#
if [ ! -d "$sourceFolder" ]
then
  echo "Error: Source folder ($sourceFolder) not found"
  echo -e "Enter the path to create the projects source folder (current: '$sourceFolder'): "

  if [ $runSilent == false ]; 
  	then
  		read srcFolder
	else
		echo -e "\n *** Running in silent mode *** \n"
  fi
	
  if [[ $srcFolder != "" ]]; then
    sourceFolder="$srcFolder"
  fi
  mkdir -p "$sourceFolder"
  [ ! -d "$sourceFolder" ] && { exit 1; }
fi
echo "...Project source folder: $sourceFolder"
cd "$sourceFolder"

#-----  DESTINATION FOLDER  -----#
if [[ $destinationFolder =~ $re ]]; then
  echo "...remove trailing slash '/' from destination folder..."
  destinationFolder="${BASH_REMATCH[1]}";
fi
if [ -d "$destinationFolder" ]
then
  echo "...Destination ($destinationFolder) exists - clearing..."
  rm -f "$destinationFolder/*"
  echo "...$destinationFolder cleared."
else
  #echo "Error: Destination ($destinationFolder) not found"
	echo "current version: $currentVersionStr"
  if [[ "$destinationFolder" == "" ]]
  then
    dest="$rootFolder/dist/RM_"
    curDate=$(date +"%Y%m%d")
    versionStr="_v$currentVersionStr"
    destinationFolder="$dest$curDate$versionStr"
    echo "...defaulting destination folder to: $destinationFolder"
  fi

  echo -n "Enter the destination path for the deployment artifacts (currently: '$destinationFolder'): "
  if [ $runSilent == false ]; 
  	then
  		read destFolder
	else
		echo -e "\n *** Running in silent mode *** \n"
  fi

  if [[ $destFolder != "" ]]; then
    destinationFolder="$destFolder"
  fi

  
  mkdir -p "$destinationFolder"
  if [ ! -d "$destinationFolder" ]
  then
	echo -e "Destination folder does not exist..."
    exit 1;
  fi
fi
echo "...WAR files destination folder: $destinationFolder"



primeDocumentation "RM Build ($currentVersionStr) -- $(date +"%Y%m%d")"

echo " "
echo "     **********     **********     **********     **********     **********"
echo " "


#************************#
#****  Finsurv Rules ****#
if [ -d "$sourceFolder/finsurv-rules" ] 
then
	echo "Deleting : finsurv-rules..."
	rm -rf "$sourceFolder/finsurv-rules"
fi
checkoutRepo $sourceFolder "finsurv-rules" "git@bitbucket.org:synthesis_admin/finsurv-rules.git"
if [ -d "$sourceFolder/finsurv-rules" ]
then
  echo "Building : finsurv-rules..."
  cd finsurv-rules
  git checkout master
  git pull
  #npm install
  #npm run build
	getDocumentation "finsurv-rules"
	tagVersion
fi
cd "$workingDir"
cd "$sourceFolder"
echo " "
echo "     **********     **********     **********     **********     **********"
echo " "

#********************#
#****  BopServer ****#
if [ -d "$sourceFolder/bopserver" ] 
then
	echo "Deleting : bopserver..."
	rm -rf "$sourceFolder/bopserver"
fi
checkoutRepo $sourceFolder "bopserver" "git@bitbucket.org:synthesis_admin/bopserver.git"
if [ -d "$sourceFolder/bopserver" ]
then
  echo "Building : bopserver..."
  cd bopserver
  git submodule update --init --recursive
  git checkout -- package-lock.json
  git checkout -- package.json
  git checkout master
  git pull
	getDocumentation "bopserver"
	#tagVersion
  cd electronic-bop-form
  git checkout -- package-lock.json
  git checkout -- package.json
  git checkout master
  git pull
	getDocumentation "bopserver/electronic-bop-form"
	#tagVersion
  cd rules
  git checkout -- package-lock.json
  git checkout -- package.json
  git checkout master
  git pull
	getDocumentation "bopserver/electronic-bop-form/rules"
	tagVersion
  cd ../
	tagVersion
  cd ../
	tagVersion
  npm install
  npm run build
fi
cd "$workingDir"
cd "$sourceFolder"
echo " "
echo "     **********     **********     **********     **********     **********"
echo " "



# ****************************#
# ****  Rules Java Engine ****#
checkoutRepo $sourceFolder "finsurv-rules-java-engine" "git@bitbucket.org:synthesis_admin/finsurv-rules-java-engine.git"
if [ -d "$sourceFolder/finsurv-rules-java-engine" ]
then
  echo "Building : finsurv-rules-java-engine..."
  cd finsurv-rules-java-engine
  git pull
  git checkout "Version1"
	getDocumentation "finsurv-rules-java-engine"
	tagVersion
  gradle clean
  gradle publishToMavenLocal
fi
cd "$workingDir"
cd "$sourceFolder"
echo " "
echo "     **********     **********     **********     **********     **********"
echo " "

#***************************#
#****  Rules Management ****#
checkoutRepo $sourceFolder "rules-management" "git@bitbucket.org:synthesis_admin/rules-management.git"
if [ -d "$sourceFolder/rules-management" ]
then
  echo "Building : rules-management..."
  cd rules-management
  git pull

  #*** Copy all the resources to the intelliJ "out" compilation folder ***#
  rm -rf out/production/resources/*
  cp -r build/resources/main/* out/production/resources/
	#*** Remove all stale, build artefacts (clean doesn't always seem to work)...
	rm -rf build/resources

	getDocumentation "rules-management"
	tagVersion
  gradle processResources
  #gradle build
  gradle --refresh-dependencies
  gradle clean
  gradle war
fi
cd "$workingDir"
cd "$sourceFolder"
echo " "
echo "     **********     **********     **********     **********     **********"
echo " "


echo "...build complete."
echo " "
echo "     **********     **********     **********     **********     **********"
echo " "


#--------------------------------------------------------------#
#----  COPY THE BUILD FILES TO SOME FOLDER FOR DEPLOYMENT  ----#
#clear
#echo "...copying war files to destination folder: $destinationFolder"
echo "***** Remaining WAR files => $destinationFolder *****"

ls "$destinationFolder"
currentWar="$sourceFolder/rules-management/build/libs"
echo "	Rules Management WAR... $currentWar -> $destinationFolder"
ls "$currentWar"
cp -f "$currentWar"/*.war "$destinationFolder"
echo "***** WAR files copied. *****"
echo ""
echo ""
echo ""
echo "...WARs Complete."
echo ""
echo ""
echo ""

cd "$workingDir"
echo " "
echo "     **********     **********     **********     **********     **********"
echo " "



#-------------------------------------------------------------#
#----  BUILD AND COPY JAR FILES TO FOLDER FOR DEPLOYMENT  ----#
#clear
echo "***** Building and copying JAR files => $destinationFolder/jars/... *****"
echo ""
echo ""
echo ""

#***************************************#
##****  Rules Java Engine -- Java 6  ****#
#if [ -d "$sourceFolder/finsurv-rules-java-engine" ]
#then
#  cd "$sourceFolder/finsurv-rules-java-engine"
#  echo "building : finsurv-rules-java-engine - Java 6..."
#  gradle -b build16.gradle build
#  echo "...build instruction complete."
#fi
#cd "$workingDir"
#cd "$sourceFolder"
#
#mkdir "$destinationFolder/OFFLINE_RULES_ENGINE_JARS"
#currentJar="$sourceFolder/finsurv-rules-java-engine/build/libs"
#echo "Copying finsurv-rules-java-engine JAR files to deployment folder..."
#ls "$currentJar"
#cp -f "$currentJar"/*.jar "$destinationFolder/OFFLINE_RULES_ENGINE_JARS/"
#echo "***** JAR files copied. *****"

##** remove any source JARS that were copied over  **#
#cd "$destinationFolder/OFFLINE_RULES_ENGINE_JARS"
#rm -f *source*.jar
#echo "***** Source JARs removed. *****"

echo ""
echo ""
echo ""
echo "...JAR files build and copy - complete."
echo ""
echo ""
echo ""

cd "$workingDir"
echo " "
echo "     **********     **********     **********     **********     **********"
echo " "


echo " "
echo "     **********     **********     **********     **********     **********"
echo " "

echo ""
echo ""
echo ""
echo "...Build Complete."

closeDocumentation 