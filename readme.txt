Rules Management
================
The rules management application provides a single point of access to the all the regulatory rules for an organisation.

Configuration
-------------
The build script relies on having the /finsurv-rules repo checked out on the same level as /rules-management repo. It'll then pick up the rules files from the reg_rule_repo directory /finsurv-rules/reg_rule_repo.

Currently this directory is available on the Cleanup branch of the finsurv-rules repository.