package za.co.synthesis.regulatory.rules.rest;

import org.springframework.web.bind.annotation.ExceptionHandler;
import za.co.synthesis.rule.core.EvaluationException;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by jake on 3/31/16.
 */
public class ControllerBase {
    @ExceptionHandler(EvaluationException.class)
    public void handleEvaluationException(HttpServletResponse response, EvaluationException e) throws IOException {
        response.setStatus(500);
        response.getWriter().print("{errorType=\"EvaluationException\", \"message\": \"" + e.getMessage() + "\"}");
    }

    @ExceptionHandler(Exception.class)
    public void handleException(HttpServletResponse response, Exception e) throws IOException {
        response.setStatus(500);
        response.getWriter().print("{errorType=\"Exception\", \"message\": \"" + e.getMessage() + "\"}");
    }
}
