package za.co.synthesis.regulatory.rules.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import za.co.synthesis.regulatory.rules.schema.*;
import za.co.synthesis.regulatory.rules.support.EvaluationPackageFactory;
import za.co.synthesis.regulatory.rules.support.LookupsPackageFactory;
import za.co.synthesis.regulatory.rules.support.PackageFactory;
import za.co.synthesis.regulatory.rules.support.ValidationPackageFactory;

/**
 * User: jake
 * Date: 5/1/16
 * Time: 8:53 AM
 * This REST service provides access to all the evaluation, validation and lookup rules that are each consolidated into
 * a single file based on the configuration system configuration
 */
@Controller
@RequestMapping("/rules")
public class Rules {
  Logger logger = LoggerFactory.getLogger(Rules.class);

  private String templateName = "AMD";

  @Autowired
  private PackageFactory packageFactory;

  @Autowired
  private ValidationPackageFactory validationPackageFactory;

  @Autowired
  private EvaluationPackageFactory evaluationPackageFactory;

  @Autowired
  private LookupsPackageFactory lookupsPackageFactory;

  @RequestMapping(value = "/{packageName}", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
  public @ResponseBody
  PackageWithEngineVersion getPackage(@PathVariable String packageName) throws Exception {
    PackageFactory.PackagePack packagePack = packageFactory.getPackagePack(packageName);

    return new PackageWithEngineVersion(packagePack);
  }

  @RequestMapping(value = "/{packageName}/validation", method = RequestMethod.GET, produces = "application/javascript;charset=UTF-8")
  public @ResponseBody String getValidations(@PathVariable String packageName) throws Exception {
    return validationPackageFactory.getPackage(packageName, ValidationPackageFactory.Location.Validation, templateName);
  }

  @RequestMapping(value = "/{packageName}/document", method = RequestMethod.GET, produces = "application/javascript;charset=UTF-8")
  public @ResponseBody String getDocuments(@PathVariable String packageName) throws Exception {
    return validationPackageFactory.getPackage(packageName, ValidationPackageFactory.Location.Document, templateName);
  }

  @RequestMapping(value = "/{packageName}/evaluation", method = RequestMethod.GET, produces = "application/javascript;charset=UTF-8")
  public @ResponseBody String getEvaluations(@PathVariable String packageName) throws Exception {
    return evaluationPackageFactory.getPackage(packageName, templateName);
  }

  @RequestMapping(value = "/{packageName}/lookups", method = RequestMethod.GET, produces = "application/javascript;charset=UTF-8")
  public @ResponseBody String getLookups(@PathVariable String packageName) throws Exception {
    return lookupsPackageFactory.getPackage(packageName, templateName);
  }
}
