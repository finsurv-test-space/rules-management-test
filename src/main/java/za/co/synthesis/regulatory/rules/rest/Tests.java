package za.co.synthesis.regulatory.rules.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import za.co.synthesis.regulatory.rules.support.TestPackageFactory;


@Controller
@RequestMapping("/tests")
public class Tests {
  Logger logger = LoggerFactory.getLogger(Tests.class);

  @Autowired
  private TestPackageFactory testPackageFactory;

  @RequestMapping(value = "/{packageName}/{testfile}", method = RequestMethod.GET, produces = "application/javascript;charset=UTF-8")
  public @ResponseBody String getTestFile(@PathVariable String packageName, @PathVariable String testfile) throws Exception {
    return testPackageFactory.getPackageTest(packageName, testfile);
  }
}
