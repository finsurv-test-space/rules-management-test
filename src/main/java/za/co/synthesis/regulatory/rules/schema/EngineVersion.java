package za.co.synthesis.regulatory.rules.schema;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by james on 2017/07/06.
 */
public class EngineVersion {
    private za.co.synthesis.rule.core.EngineVersion internal_version;

    public EngineVersion(za.co.synthesis.rule.core.EngineVersion internal_version) {
        this.internal_version = internal_version;
    }

    @JsonProperty("Major")
    public int getMajor() {
        return internal_version.getMajor();
    }

    @JsonProperty("Minor")
    public int getMinor() {
        return internal_version.getMinor();
    }
}
