package za.co.synthesis.regulatory.rules.schema;

import com.fasterxml.jackson.annotation.JsonProperty;
import za.co.synthesis.regulatory.rules.support.PackageFactory;

import java.util.List;

/**
 * Created by james on 2017/07/06.
 */
public class Package {
    private final PackageFactory.CompositePackage internal_package;

    public Package(PackageFactory.CompositePackage internal_package) {
        this.internal_package = internal_package;
    }

    @JsonProperty("Name")
    public String getName() {
        return internal_package.getName();
    }

    @JsonProperty("Parent")
    public Package getParent() {
        return internal_package.getParent() != null ? new Package(internal_package.getParent()) : null;
    }

    @JsonProperty("Features")
    public List<String> getFeatures() {
        return internal_package.getFeatures();
    }
}
