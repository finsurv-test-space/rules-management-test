package za.co.synthesis.regulatory.rules.schema;

import com.fasterxml.jackson.annotation.JsonProperty;
import za.co.synthesis.regulatory.rules.support.PackageFactory;

import java.util.Map;

/**
 * Created by james on 2017/07/06.
 */
public class PackageWithEngineVersion {
    private final PackageFactory.PackagePack packagePack;

    public PackageWithEngineVersion(PackageFactory.PackagePack packagePack) {
        this.packagePack = packagePack;
    }

    @JsonProperty("Package")
    public Package getPackage() {
        return new Package(packagePack.getPackage());
    }

    @JsonProperty("ValidationEngineVersion")
    public EngineVersion getValidationEngineVersion() {
        return new EngineVersion(packagePack.getVersions().getValidationVersion());
    }

    @JsonProperty("EvaluationEngineVersion")
    public EngineVersion getEvaluationEngineVersion() {
        return new EngineVersion(packagePack.getVersions().getEvaluationVersion());
    }

    @JsonProperty("Mapping")
    public Map<String, String> getMapping() {
        return packagePack.getMapping();
    }
}
