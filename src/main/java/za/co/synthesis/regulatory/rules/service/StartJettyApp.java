package za.co.synthesis.regulatory.rules.service;

/**
 * Created by petruspretorius on 08/06/2017.
 */

import org.apache.commons.cli.*;
import org.eclipse.jetty.server.Connector;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.servlet.DispatcherServlet;
import za.co.synthesis.regulatory.rules.support.DefaultRedirect;

import java.io.File;
import java.lang.reflect.Method;
import java.net.URI;
import java.net.URL;
import java.net.URLClassLoader;

/**
 * Created by jake on 3/31/16.
 */
public class StartJettyApp {
  //need to do add path to Classpath with reflection since the URLClassLoader.addURL(URL url) method is protected:
  public static void addPathToClasspath(String s) throws Exception {
    File f = new File(s);
    URI u = f.toURI();
    URLClassLoader urlClassLoader = (URLClassLoader) ClassLoader.getSystemClassLoader();
    Class<URLClassLoader> urlClass = URLClassLoader.class;
    Method method = urlClass.getDeclaredMethod("addURL", new Class[]{URL.class});
    method.setAccessible(true);
    method.invoke(urlClassLoader, new Object[]{u.toURL()});
  }

  private static Options getOptions() {
    Options options = new Options();
    Option config = Option.builder("repo")
        .hasArg()
        .desc("the configuration file (classpath:reg_rule_repo)")
        .build();
    options.addOption(config);

    Option customConfig = Option.builder("custom_repo")
            .hasArg()
            .desc("the custom configuration file (ie: file:c:\\\\customRepo)")
            .build();
    options.addOption(customConfig);

    Option port = Option.builder("port")
        .hasArg()
        .desc("the post to listen on (default 8080)")
        .build();
    options.addOption(port);

    Option confidentialPort = Option.builder("confidentialPort")
        .hasArg()
        .desc("the post to listen on (default 8443)")
        .build();
    options.addOption(confidentialPort);

    Option help = Option.builder("help")
        .desc("provide help")
        .build();
    options.addOption(help);

    return options;
  }


  public static void main(String[] args) throws Exception {
    Options options = getOptions();
    // create the parser
    CommandLineParser parser = new DefaultParser();
    CommandLine cmdline;
    try {
      // parse the command line arguments
      cmdline = parser.parse(options, args);

      if (cmdline.hasOption("help")) {
        HelpFormatter formatter = new HelpFormatter();
        formatter.printHelp("rules-data-store", options);
        return;
      }
    } catch (ParseException e) {
      System.err.println("Commandline parameters not valid.  Reason: " + e.getMessage());

      HelpFormatter formatter = new HelpFormatter();
      formatter.printHelp("rules-management", options);
      return;
    }

    int port = 8082;
    int confidentialPort = 8443;
    if (cmdline.hasOption("port")) {
      port = Integer.parseInt(cmdline.getOptionValue("port"));
    }
    if (cmdline.hasOption("confidentialPort")) {
      confidentialPort = Integer.parseInt(cmdline.getOptionValue("confidentialPort"));
    }

    // This property is expected by the rules-management-spring.xml file so we set it
    //System.setProperty("rules-management.repo", cmdline.getOptionValue("config", "file:../config/config.properties"));

    // We expect the startip directory to be set to the base txstream folder
    String resourcesDir = "src/main/resources";
    String webappDir = "src/main/webapp";


    Server server = new Server();

    ServerConnector connector = new ServerConnector(server, 10, 0);
    connector.setPort(port);

    server.setConnectors(new Connector[]{connector});

    // Adding the resources directory to the classpath at runtime to allow Spring to be able to load files
    // from the resource directory for testing purposes I.e. Help Spring Framework help me
    addPathToClasspath(resourcesDir);

    String repo = cmdline.getOptionValue("repo", "classpath:/reg_rule_repo");
    System.out.println("Using repo: -- " + repo);

    String custom_repo = cmdline.getOptionValue("custom_repo", "");
    System.out.println("Using custom repo: -- " + custom_repo);

    ServletContextHandler handler = new ServletContextHandler(server, "/rules-management", true, false);
    handler.setInitParameter("rules-management.repo", repo);
    handler.setInitParameter("rules-management.custom", custom_repo);

    handler.setInitParameter("contextConfigLocation", "classpath:/config/rules-management-spring.xml");
    handler.addEventListener(new SystemsPropertyHelper());
    handler.addEventListener(new ContextLoaderListener());

    ServletHolder servletHolder = new ServletHolder("api", DispatcherServlet.class);
    servletHolder.setInitParameter("contextClass", "org.springframework.web.context.support.AnnotationConfigWebApplicationContext");
    servletHolder.setInitParameter("contextConfigLocation", "za.co.synthesis.regulatory.rules.service.WebAppConfig");
    handler.addServlet(servletHolder, "/api/*");

    servletHolder = new ServletHolder("redirect", DefaultRedirect.class);
    handler.addServlet(servletHolder, "/");

    server.start();
  }
}
