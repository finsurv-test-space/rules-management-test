package za.co.synthesis.regulatory.rules.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jndi.JndiTemplate;

import javax.naming.NamingException;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import java.util.Enumeration;

/**
 * Created by jake on 5/27/16.
 */
public class SystemsPropertyHelper implements ServletContextListener {
  Logger logger = LoggerFactory.getLogger(SystemsPropertyHelper.class);

  public void contextInitialized(ServletContextEvent event) {
    ServletContext context = event.getServletContext();
    Enumeration<String> params = context.getInitParameterNames();

    String repoName = "rules-management.repo";
    String customName = "rules-management.custom";

    boolean hasRepo = false;
    boolean hasCustom = false;
    while (params.hasMoreElements()) {
      String param = (String) params.nextElement();
      String value = context.getInitParameter(param);
      if (param.equals(repoName)) {
        hasRepo = true;
        System.setProperty(param, value);
      }
      if (param.equals(customName)) {
        hasCustom = true;
        System.setProperty(param, value);
      }
    }

    String container_prefix = "java:comp/env/";
    JndiTemplate jndiTemplate = new JndiTemplate();
    try {
      Object objRepo = jndiTemplate.lookup(container_prefix + repoName);
      if (objRepo != null) {
        hasRepo = true;
        System.setProperty(repoName, objRepo.toString());
        logger.info("Environment: "+ repoName + " = " + objRepo.toString());
      }
    } catch (NamingException e) {
      logger.warn("No Environment parameter " + repoName + "found");
    }
    try {
      Object objCustom = jndiTemplate.lookup(container_prefix + customName);
      if (objCustom != null) {
        hasCustom = true;
        System.setProperty(customName, objCustom.toString());
        logger.info("Environment: " + customName + " = " + objCustom.toString());
      }
    } catch (NamingException e) {
      logger.warn("No Environment parameter " + customName + "found");
    }

    if (!hasRepo) {
      System.setProperty(repoName, "classpath:/reg_rule_repo");
    }
    if (!hasCustom) {
      System.setProperty(customName, "");
    }
  }

  public void contextDestroyed(ServletContextEvent event) {
  }
}