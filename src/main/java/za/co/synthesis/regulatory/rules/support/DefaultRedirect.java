package za.co.synthesis.regulatory.rules.support;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by jake on 5/27/16.
 */
public class DefaultRedirect extends HttpServlet {
  public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    resp.setContentType("text/html");
    resp.setStatus(HttpServletResponse.SC_MOVED_TEMPORARILY);
    resp.setHeader("Location", "api/swagger-ui.html");
  }
}
