package za.co.synthesis.regulatory.rules.support;

import za.co.synthesis.javascript.JSArray;
import za.co.synthesis.javascript.JSObject;
import za.co.synthesis.javascript.JSWriter;
import za.co.synthesis.rule.core.EngineVersion;
import za.co.synthesis.rule.evaluation.Context;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: jake
 * Date: 5/6/16
 * Time: 10:14 AM
 * Given a set of evaulation rule array compose a JavaScript file using the AMD template
 */
public class EvaluationAMDTemplate implements IEvaluationTemplate {
  private StringBuilder composedEvaluationRules(EngineVersion version, Map<String, String> mapping,
                                                JSObject evalContext, List<JSArray> evalRulesList,
                                                List<JSArray> assumptionsList,
                                                List<JSArray> scenarioList, String comment) {
    List<String> varRuleNames = new ArrayList<String>();
    List<String> varAssumptionNames = new ArrayList<String>();
    List<String> varScenarioNames = new ArrayList<String>();

    int v = 0;
    for (int i=0; i<evalRulesList.size(); i++) {
      varRuleNames.add("v"+ (++v));
    }

    for (int i=0; i<assumptionsList.size(); i++) {
      varAssumptionNames.add("v"+ (++v));
    }

    for (int i=0; i<scenarioList.size(); i++) {
      varScenarioNames.add("v"+ (++v));
    }

    StringBuilder sb = new StringBuilder();
    sb.append("define(function () { return function(evaluationEx){").append("\n");
    for (String varName : varRuleNames) {
      sb.append("  var ").append(varName).append(";\n");
    }
    for (String varName : varAssumptionNames) {
      sb.append("  var ").append(varName).append(";\n");
    }
    for (String varName : varScenarioNames) {
      sb.append("  var ").append(varName).append(";\n");
    }
    sb.append("  with (evaluationEx) {").append("\n");

    if (evalContext != null) {
      sb.append("    var ctx = ");

      JSWriter writer = new JSWriter();
      writer.setLinePrefix("    ");
      writer.setNewline("\n");
      writer.setIndent("  ");
      evalContext.compose(writer);

      sb.append(writer.toString());
      sb.append(";\n");
    }
    for (int i=0; i<evalRulesList.size(); i++) {
      sb.append("    ").append(varRuleNames.get(i)).append(" = ");

      JSWriter writer = new JSWriter();
      writer.setLinePrefix("    ");
      writer.setNewline("\n");
      writer.setIndent("  ");
      evalRulesList.get(i).compose(writer);

      sb.append(writer.toString());
      sb.append(";\n");
    }

    for (int i=0; i<assumptionsList.size(); i++) {
      sb.append("    ").append(varAssumptionNames.get(i)).append(" = ");

      JSWriter writer = new JSWriter();
      writer.setLinePrefix("    ");
      writer.setNewline("\n");
      writer.setIndent("  ");
      assumptionsList.get(i).compose(writer);

      sb.append(writer.toString());
      sb.append(";\n");
    }

    for (int i=0; i<scenarioList.size(); i++) {
      sb.append("    ").append(varScenarioNames.get(i)).append(" = ");

      JSWriter writer = new JSWriter();
      writer.setLinePrefix("    ");
      writer.setNewline("\n");
      writer.setIndent("  ");
      scenarioList.get(i).compose(writer);

      sb.append(writer.toString());
      sb.append(";\n");
    }
    sb.append("  ").append("}\n");

    sb.append("  return {\n");
    sb.append("    comment: '").append(comment).append("',\n");
    sb.append("    engine: {major: '").append(version.getMajor()).append("', minor: '").append(version.getMinor()).append("'},\n");
    if (evalContext != null) {
      sb.append("    context: ctx,\n");
    }
    sb.append("    rules: [");
    boolean bFirst = true;
    for (String varName : varRuleNames) {
      if (bFirst) {
        bFirst = false;
      }
      else {
        sb.append(", ");
      }
      sb.append(varName);
    }
    sb.append("]");
    if (varAssumptionNames.size() > 0) {
      sb.append(",\n");
      sb.append("    assumptions: [");
      bFirst = true;
      for (String varName : varAssumptionNames) {
        if (bFirst) {
          bFirst = false;
        } else {
          sb.append(", ");
        }
        sb.append(varName);
      }
      sb.append("]");
    }
    if (varScenarioNames.size() > 0) {
      sb.append(",\n");
      sb.append("    scenarios: [");
      bFirst = true;
      for (String varName : varScenarioNames) {
        if (bFirst) {
          bFirst = false;
        } else {
          sb.append(", ");
        }
        sb.append(varName);
      }
      sb.append("]");
    }
    if (mapping != null && mapping.size() > 0) {
      sb.append(",\n");
      sb.append("    mappings: {");
      bFirst = true;
      for (Map.Entry<String, String> entry : mapping.entrySet()) {
        if (bFirst) {
          bFirst = false;
        }
        else {
          sb.append(", ");
        }
        sb.append(entry.getKey()).append(": \"").append(entry.getValue()).append("\"");
      }
      sb.append("}");
    }
    sb.append("\n");
    sb.append("  };\n");
    sb.append("}})\n");

    return sb;
  }

  @Override
  public String compose(EngineVersion version, Map<String, String> mapping,
                        JSObject evalContext, List<JSArray> evalRulesList, List<JSArray> assumptionsList, List<JSArray> scenarioList, String comment) {
    return composedEvaluationRules(version, mapping, evalContext, evalRulesList, assumptionsList, scenarioList, comment).toString();
  }
}
