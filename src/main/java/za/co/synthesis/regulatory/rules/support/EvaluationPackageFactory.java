package za.co.synthesis.regulatory.rules.support;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.core.io.Resource;
import za.co.synthesis.javascript.JSArray;
import za.co.synthesis.javascript.JSObject;
import za.co.synthesis.javascript.JSStructureParser;
import za.co.synthesis.javascript.JSUtil;
import za.co.synthesis.rule.core.EvaluationException;
import za.co.synthesis.rule.support.JSConstant;

import java.io.File;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * User: jake
 * Date: 5/6/16
 * Time: 10:20 AM
 * Provides access to the composed set of evaluation rules for a given package using a specific template
 */
public class EvaluationPackageFactory implements ApplicationContextAware {
  private static final Map<String, IEvaluationTemplate> templateMap = new HashMap<String, IEvaluationTemplate>();

  static {
    registerTemplate("AMD", new EvaluationAMDTemplate());
  }

  public static synchronized void registerTemplate(String templateName, IEvaluationTemplate template) {
    templateMap.put(templateName, template);
  }

  private PackageFactory packageFactory;
  private ApplicationContext appCtx;

  @Override
  public void setApplicationContext(ApplicationContext appCtx) throws BeansException {
    this.appCtx = appCtx;
  }

  public void setPackageFactory(PackageFactory packageFactory) {
    this.packageFactory = packageFactory;
  }

  public String getPackage(String packageName, String template) throws EvaluationException {
    if (templateMap.containsKey(template)) {
      return composePackage(packageName, templateMap.get(template));
    }
    else
      throw new EvaluationException("No validation template called '" + template + "' has been registered");
  }

  private String composePackage(String packageName, IEvaluationTemplate template) throws EvaluationException {
    PackageFactory.PackagePack packagePack;
    try {
      packagePack = packageFactory.getPackagePack(packageName);
    } catch (Exception e) {
      throw new EvaluationException("Error getting PackagePack", e);
    }
    List<JSArray> evalRulesList = new ArrayList<JSArray>();
    List<JSArray> assumptionsList = new ArrayList<JSArray>();
    List<JSArray> scenarioList = new ArrayList<JSArray>();
    JSObject evalContext = loadRuleArrayFromPackagePack(packagePack, evalRulesList, assumptionsList, scenarioList);

    return template.compose(packagePack.getVersions().getEvaluationVersion(),
            packagePack.getMapping(), evalContext, evalRulesList, assumptionsList, scenarioList, packagePack.getEvaluationPackagePath());
  }

  private void loadRuleListFromResource(final String resourceName, List<JSArray> evalRulesList) throws EvaluationException {
    try {
      Resource resource = appCtx.getResource(resourceName);
      if (resource.exists()) {
        InputStreamReader reader = new InputStreamReader(resource.getInputStream());
        JSStructureParser parser = new JSStructureParser(reader);
        Object obj = parser.parse();

        List<JSObject> evalRules = new ArrayList<JSObject>();
        JSUtil.findObjectsWith(obj, "drResStatus", evalRules);

        JSArray jsRuleset = new JSArray();
        for (JSObject evalRule : evalRules) {
          jsRuleset.add(evalRule);
        }
        evalRulesList.add(jsRuleset);
      }
    }
    catch (Exception e) {
      throw new EvaluationException("Could not parse '" + resourceName + "' rule file", e);
    }
  }

  private JSObject loadContextFromResource(final String resourceName) throws EvaluationException {
    JSObject resultContext = null;
    try {
      Resource resource = appCtx.getResource(resourceName);
      if (resource.exists()) {
        InputStreamReader reader = new InputStreamReader(resource.getInputStream());
        JSStructureParser parser = new JSStructureParser(reader);
        Object obj = parser.parse();

        resultContext = JSUtil.findObjectWith(obj, "whoAmIRegex");
      }
    }
    catch (Exception e) {
      throw new EvaluationException("Could not parse '" + resourceName + "' rule file", e);
    }
    return resultContext;
  }

  private void loadAssumptionsFromResource(final String resourceName, List<JSArray> assumptionsList) throws EvaluationException {
    try {
      Resource resource = appCtx.getResource(resourceName);
      if (resource.exists()) {
        InputStreamReader reader = new InputStreamReader(resource.getInputStream());
        JSStructureParser parser = new JSStructureParser(reader);
        Object obj = parser.parse();

        List<JSObject> evalRules = new ArrayList<JSObject>();
        JSUtil.findObjectsWith(obj, "knownSide", evalRules);

        JSArray jsRuleset = new JSArray();
        for (JSObject evalRule : evalRules) {
          jsRuleset.add(evalRule);
        }
        assumptionsList.add(jsRuleset);
      }
    }
    catch (Exception e) {
      throw new EvaluationException("Could not parse '" + resourceName + "' rule file", e);
    }
  }

  private void loadScenariosFromResource(final String resourceName, List<JSArray> scenariosList) throws EvaluationException {
    try {
      Resource resource = appCtx.getResource(resourceName);
      if (resource.exists()) {
        InputStreamReader reader = new InputStreamReader(resource.getInputStream());
        JSStructureParser parser = new JSStructureParser(reader);
        Object obj = parser.parse();

        List<JSObject> evalRules = new ArrayList<JSObject>();
        JSUtil.findObjectsWith(obj, "match", evalRules);

        JSArray jsRuleset = new JSArray();
        for (JSObject evalRule : evalRules) {
          jsRuleset.add(evalRule);
        }
        scenariosList.add(jsRuleset);
      }
    }
    catch (Exception e) {
      throw new EvaluationException("Could not parse '" + resourceName + "' rule file", e);
    }
  }

  private JSObject loadEvalRulesFromPath(final String packagepath, List<JSArray> evalRulesList,
                                         List<JSArray> assumptionsList,
                                         List<JSArray> scenarioList) throws EvaluationException {
    String dataPath = packagepath + File.separator + "evaluation";
    String nameEvalRules = dataPath + File.separator + "evalRules.js";
    String nameEvalAssumptions = dataPath + File.separator + "evalAssumptions.js";
    String nameEvalScenarios = dataPath + File.separator + "evalScenarios.js";
    String nameEvalContext = dataPath + File.separator + "evalContext.js";
    loadRuleListFromResource(nameEvalRules, evalRulesList);
    loadAssumptionsFromResource(nameEvalAssumptions, assumptionsList);
    loadScenariosFromResource(nameEvalScenarios, scenarioList);
    return loadContextFromResource(nameEvalContext);
  }

  /**
   * This will load rules from a package path.
   * @param packagePack is the package to load rules for
   * @param evalRulesList the returned list of rule arrays
   * @param assumptionsList the returned list of rule assumptions
   * @return the JSObject associated with the Evaluation Context
   * @throws EvaluationException
   */
  public JSObject loadRuleArrayFromPackagePack(final PackageFactory.PackagePack packagePack,
                                               List<JSArray> evalRulesList, List<JSArray> assumptionsList,
                                               List<JSArray> scenarioList) throws EvaluationException {
    JSObject resultContext = null;
    PackageFactory.CompositePackage pack = packagePack.getPackage();
    while (pack != null) {
      JSObject evalContext = loadEvalRulesFromPath(pack.getPath(), evalRulesList, assumptionsList, scenarioList);
      if (resultContext == null)
        resultContext = evalContext;
      pack = pack.getParent();
    }
    return resultContext;
  }
}
