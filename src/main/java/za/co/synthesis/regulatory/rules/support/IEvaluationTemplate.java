package za.co.synthesis.regulatory.rules.support;

import za.co.synthesis.javascript.JSArray;
import za.co.synthesis.javascript.JSObject;
import za.co.synthesis.rule.core.EngineVersion;
import za.co.synthesis.rule.evaluation.Context;

import java.util.List;
import java.util.Map;

/**
 * User: jake
 * Date: 5/6/16
 * Time: 10:00 AM
 * Given a list of JSArrays compose a file with all the evaluation rules
 */
public interface IEvaluationTemplate {
  String compose(EngineVersion version, Map<String, String> mapping,
                 JSObject evalContext, List<JSArray> evalRulesList, List<JSArray> assumptionsList, List<JSArray> scenarioList, String comment);
}
