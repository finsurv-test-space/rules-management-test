package za.co.synthesis.regulatory.rules.support;

import za.co.synthesis.javascript.JSObject;

import java.util.List;


/**
 * User: jake
 * Date: 5/7/16
 * Time: 7:02 PM
 * Given an object that supports an ILookups interface compose a file with all the lookups
 */
public interface ILookupsTemplate {
  String compose(List<JSObject> lookupObjs, String comment);
}
