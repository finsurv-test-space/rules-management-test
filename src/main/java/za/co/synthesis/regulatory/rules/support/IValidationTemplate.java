package za.co.synthesis.regulatory.rules.support;

import za.co.synthesis.javascript.JSObject;
import za.co.synthesis.rule.core.EngineVersion;

import java.util.List;
import java.util.Map;

/**
 * User: jake
 * Date: 5/6/16
 * Time: 8:28 AM
 * Given a list of JSObjects compose a file with all the validation rules
 */
public interface IValidationTemplate {
  String compose(EngineVersion version, Map<String, String> mapping, List<JSObject> rulesetObjs, String comment);
}
