package za.co.synthesis.regulatory.rules.support;

import za.co.synthesis.javascript.JSObject;

import java.util.ArrayList;
import java.util.List;

/**
 * User: jake
 * Date: 5/7/16
 * Time: 7:05 PM
 * Given an object that supports an ILookups interface compose a JavaScript file using the AMD template
 */
public class LookupsAMDTemplate implements ILookupsTemplate {
  private StringBuilder composedLookups(List<JSObject> lookupObjs, String comment) {
    List<String> doneLookups = new ArrayList<String>();

    StringBuilder sb = new StringBuilder();
    sb.append("define({\n");
    sb.append("    comment: '").append(comment).append("',\n");
    sb.append("    lookups: {\n");

    boolean bFirst = true;
    for (JSObject lookups : lookupObjs) {
      for (String lookupName : lookups.keySet()) {
        if (!doneLookups.contains(lookupName)) {
          doneLookups.add(lookupName);
          if (bFirst) {
            bFirst = false;
          }
          else {
            sb.append(",\n");
          }

          sb.append("      ").append(lookupName).append(": ");
          sb.append(lookups.get(lookupName).toString());
        }
      }
    }
    sb.append("\n    }\n");

    sb.append("})\n");
    return sb;
  }

  @Override
  public String compose(List<JSObject> lookupObjs, String comment) {
    return composedLookups(lookupObjs, comment).toString();
  }
}
