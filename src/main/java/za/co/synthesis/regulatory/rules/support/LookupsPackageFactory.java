package za.co.synthesis.regulatory.rules.support;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.core.io.Resource;
import za.co.synthesis.javascript.JSObject;
import za.co.synthesis.javascript.JSStructureParser;
import za.co.synthesis.javascript.JSUtil;
import za.co.synthesis.rule.core.ValidationException;

import java.io.File;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * User: jake
 * Date: 5/7/16
 * Time: 7:00 PM
 * Provides access to the composed set of lookups for a given package using a specific template
 */
public class LookupsPackageFactory implements ApplicationContextAware {
  private static final Map<String, ILookupsTemplate> templateMap = new HashMap<String, ILookupsTemplate>();

  static {
    registerTemplate("AMD", new LookupsAMDTemplate());
  }

  public static synchronized void registerTemplate(String templateName, ILookupsTemplate template) {
    templateMap.put(templateName, template);
  }

  private PackageFactory packageFactory;
  private ApplicationContext appCtx;

  @Override
  public void setApplicationContext(ApplicationContext appCtx) throws BeansException {
    this.appCtx = appCtx;
  }

  public void setPackageFactory(PackageFactory packageFactory) {
    this.packageFactory = packageFactory;
  }

  public String getPackage(String packageName, String template) throws ValidationException {
    if (templateMap.containsKey(template)) {
      return composePackage(packageName, templateMap.get(template));
    }
    else
      throw new ValidationException("No validation template called '" + template + "' has been registered");
  }

  private String composePackage(String packageName, ILookupsTemplate template) throws ValidationException {
    PackageFactory.PackagePack packagePack;
    try {
      packagePack = packageFactory.getPackagePack(packageName);
    } catch (Exception e) {
      throw new ValidationException("Error getting PackagePack", e);
    }
    List<JSObject> rulesetObjs = new ArrayList<JSObject>();
    loadLookupsFromPackagePack(packagePack, rulesetObjs);

    return template.compose(rulesetObjs, packagePack.getEvaluationPackagePath());
  }

  private void loadLookupsFromResource(final String packagepath, List<JSObject> lookupsObjs) throws ValidationException {
    String dataPath = packagepath + File.separator + "data";
    String resourceName = dataPath + File.separator + "lookups.js";
    try {
      Resource resource = appCtx.getResource(resourceName);
      if (resource.exists()) {
        InputStreamReader reader = new InputStreamReader(resource.getInputStream());
        JSStructureParser parser = new JSStructureParser(reader);
        Object obj = parser.parse();
        JSUtil.findObjectsWith(obj, "ReportingQualifier", lookupsObjs);
        JSUtil.findObjectsWith(obj, "nonResidentExceptions", lookupsObjs);
        JSUtil.findObjectsWith(obj, "residentExceptions", lookupsObjs);
        JSUtil.findObjectsWith(obj, "moneyTransferAgents", lookupsObjs);
        JSUtil.findObjectsWith(obj, "provinces", lookupsObjs);
        JSUtil.findObjectsWith(obj, "cardTypes", lookupsObjs);
        JSUtil.findObjectsWith(obj, "currencies", lookupsObjs);
        JSUtil.findObjectsWith(obj, "countries", lookupsObjs);
        JSUtil.findObjectsWith(obj, "institutionalSectors", lookupsObjs);
        JSUtil.findObjectsWith(obj, "industrialClassifications", lookupsObjs);
        JSUtil.findObjectsWith(obj, "customsOffices", lookupsObjs);
        JSUtil.findObjectsWith(obj, "categories", lookupsObjs);
        JSUtil.findObjectsWith(obj, "branches", lookupsObjs);
      }
    }
    catch (Exception e) {
      throw new ValidationException("Could not parse '" + resourceName + "' lookups file", e);
    }
  }

  /**
   * This will load rules from a package path.
   * @param packagePack is the package to load rules for
   * @param lookupsObjs the returned list of rulesets
   * @throws Exception
   */
  public void loadLookupsFromPackagePack(final PackageFactory.PackagePack packagePack,
                                         List<JSObject> lookupsObjs) throws ValidationException {
    PackageFactory.CompositePackage pack = packagePack.getPackage();
    while (pack != null) {
      loadLookupsFromResource(pack.getPath(), lookupsObjs);
      pack = pack.getParent();
    }
  }
}
