package za.co.synthesis.regulatory.rules.support;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.core.io.Resource;
import za.co.synthesis.rule.core.EngineVersion;
import za.co.synthesis.rule.core.ValidationException;
import za.co.synthesis.rule.support.RulePackage;
import za.co.synthesis.rule.support.RulePackages;

import java.io.File;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by james on 2017/07/06.
 */
public class PackageFactory implements ApplicationContextAware {
    public static class PackagePack {
        private final CompositePackage aPackage;
        private final CompositeVersions versions;
        private final Map<String, String> mapping;

        public PackagePack(CompositePackage parent_package, CompositeVersions versions, Map<String, String> mapping) {
            this.aPackage = parent_package;
            this.versions = versions;
            this.mapping = mapping;
        }

        public CompositePackage getPackage() {
            return aPackage;
        }

        public CompositeVersions getVersions() {
            return versions;
        }

        public Map<String, String> getMapping() {
            return mapping;
        }

        public String getValidationPackagePath() {
            StringBuilder sb = new StringBuilder();

            CompositePackage pack = aPackage;
            sb.append(aPackage.getName());
            while (pack != null) {
                EngineVersion ver = pack.getValidationEngine();
                sb.append(" (").append(ver.getMajor()).append(",").append(ver.getMinor()).append(")");
                if (pack.getFeatureSize() > 0) {
                    sb.append(" [");
                    for (int i = 0; i < pack.getFeatureSize(); i++) {
                        if (i > 0)
                            sb.append(", ");
                        sb.append(pack.getFeatureName(i));
                    }
                    sb.append("]");
                }
                pack = pack.getParent();
                if (pack != null)
                    sb.append(" -> ").append(pack.getName());
            }
            return sb.toString();
        }

        public String getEvaluationPackagePath() {
            StringBuilder sb = new StringBuilder();

            CompositePackage pack = aPackage;
            sb.append(aPackage.getName());
            while (pack != null) {
                EngineVersion ver = pack.getEvaluationEngine();
                sb.append(" (").append(ver.getMajor()).append(",").append(ver.getMinor()).append(")");
                pack = pack.getParent();
                if (pack != null)
                    sb.append(" -> ").append(pack.getName());
            }
            return sb.toString();
        }
    }

    public static class CompositePackage extends RulePackage {
        private final String name;
        private final String path;
        private final CompositePackage parent;
        private final List<CompositeFeature> featurePackages = new ArrayList<CompositeFeature>();

        public CompositePackage(String name, String path, RulePackage rulePackage, CompositePackage parent) {
            super(rulePackage);
            this.name = name;
            this.path = path;
            this.parent = parent;
        }

        public String getName() {
            return name;
        }

        public CompositePackage getParent() {
            return parent;
        }

        public String getPath() {
            return path;
        }

        public List<CompositeFeature> getFeaturePackages() {
            return featurePackages;
        }
    }

    public static class CompositeFeature extends RulePackage {
        private final String name;
        private final String path;

        public CompositeFeature(String name, String path, RulePackage rulePackage) {
            super(rulePackage);
            this.name = name;
            this.path = path;
        }

        public String getName() {
            return name;
        }

        public String getPath() {
            return path;
        }
    }

    public static class CompositeVersions {
        private EngineVersion validationVersion;
        private EngineVersion evaluationVersion;

        public EngineVersion getValidationVersion() {
            return validationVersion;
        }

        public void setValidationVersion(EngineVersion validationVersion) {
            this.validationVersion = validationVersion;
        }

        public EngineVersion getEvaluationVersion() {
            return evaluationVersion;
        }

        public void setEvaluationVersion(EngineVersion evaluationVersion) {
            this.evaluationVersion = evaluationVersion;
        }
    }

    private static class ExtendedRulePackage {
        private final String name;
        private final String path;
        private final RulePackage rulePackage;
        private final List<CompositeFeature> featurePackages = new ArrayList<CompositeFeature>();

        public ExtendedRulePackage(String name, String path, RulePackage rulePackage) {
            this.name = name;
            this.path = path;
            this.rulePackage = rulePackage;
        }

        public String getName() {
            return name;
        }

        public String getPath() {
            return path;
        }

        public RulePackage getRulePackage() {
            return rulePackage;
        }

        public List<CompositeFeature> getFeaturePackages() {
            return featurePackages;
        }
    }

    private static class PathedResource {
        private final String path;
        private final Resource resource;

        public PathedResource(String path, Resource resource) {
            this.path = path;
            this.resource = resource;
        }

        public String getPath() {
            return path;
        }

        public Resource getResource() {
            return resource;
        }
    }

    private String basePath;
    private String customPath;
    private ApplicationContext appCtx;

    @Override
    public void setApplicationContext(ApplicationContext appCtx) throws BeansException {
        this.appCtx = appCtx;
    }

    public void setBasePath(String basePath) {
        this.basePath = basePath;
    }

    public void setCustomPath(String customPath) {
        this.customPath = customPath;
    }

    public PackagePack getPackagePack(String packageName) throws Exception {
        List<ExtendedRulePackage> packages = new ArrayList<ExtendedRulePackage>();

        CompositeVersions versions = readPackageList(packageName, packages);

        CompositePackage pack = null;
        for (int i = packages.size()-1; i >= 0; i--) {
            ExtendedRulePackage currentPackage = packages.get(i);
            pack = new CompositePackage(currentPackage.getName(), currentPackage.getPath(), currentPackage.getRulePackage(), pack);
            pack.getFeaturePackages().addAll(currentPackage.getFeaturePackages());
        }

        Map<String, String> mapping = new HashMap<String, String>();
        for (ExtendedRulePackage ext_package : packages) {
            for (Map.Entry<String, String> entry : ext_package.getRulePackage().getMapping().entrySet()) {
                if (!mapping.containsKey(entry.getKey())) {
                    mapping.put(entry.getKey(), entry.getValue());
                }
            }
        }

        return new PackagePack(pack, versions, mapping);
    }


    private PathedResource getPackageResource(final String customRepoPath, final String repoPath, final String packageName) {
        String pathSuffix = File.separator + "package.js";
        if (customRepoPath != null && customRepoPath.length() > 0) {
            String packageFileName = customRepoPath + File.separator + packageName + pathSuffix;
            Resource resource = appCtx.getResource(packageFileName);
            if (resource.exists())
                return new PathedResource(customRepoPath + File.separator + packageName, resource);
        }

        String packageFileName = repoPath + File.separator + packageName + pathSuffix;
        Resource resource = appCtx.getResource(packageFileName);
        return new PathedResource(repoPath + File.separator + packageName, resource);
    }

    private CompositeVersions readPackageList(final String packageName,
                                 final List<ExtendedRulePackage> packages) throws Exception {
        CompositeVersions versions = new CompositeVersions();
        try {
            PathedResource resource = getPackageResource(customPath, basePath, packageName);
            if (resource.getResource().exists()) {
                InputStreamReader reader = new InputStreamReader(resource.getResource().getInputStream());

                RulePackage rule_package = RulePackages.loadRulePackage(reader);
                if (rule_package == null) {
                    throw new Exception("Could not correctly parse '" + packageName + "' package. Has it been properly defined?");
                }

                versions.setValidationVersion(rule_package.getValidationEngine());
                versions.setEvaluationVersion(rule_package.getEvaluationEngine());
                ExtendedRulePackage extRulePackage = new ExtendedRulePackage(packageName, resource.getPath(), rule_package);
                packages.add(extRulePackage);

                if (rule_package.getFeatureSize() > 0) {
                    for (int i=0; i<rule_package.getFeatureSize(); i++) {
                        String featureName = rule_package.getFeatureName(i);
                        PathedResource feature_resource = getPackageResource(customPath, basePath, featureName);
                        if (feature_resource.getResource().exists()) {
                            InputStreamReader feature_reader = new InputStreamReader(feature_resource.getResource().getInputStream());
                            RulePackage feature_rule_package = RulePackages.loadRulePackage(feature_reader);
                            extRulePackage.getFeaturePackages().add(
                                    new CompositeFeature(featureName, feature_resource.getPath(), feature_rule_package));

                            if (feature_rule_package != null) {
                                EngineVersion reqValidationVersion = feature_rule_package.getValidationEngine();
                                if (reqValidationVersion != null && reqValidationVersion.isHigher(versions.getValidationVersion())) {
                                    versions.setValidationVersion(reqValidationVersion);
                                }
                                EngineVersion reqEvaluationVersion = feature_rule_package.getValidationEngine();
                                if (reqEvaluationVersion != null && reqEvaluationVersion.isHigher(versions.getEvaluationVersion())) {
                                    versions.setEvaluationVersion(reqEvaluationVersion);
                                }
                            }
                        }
                    }
                }

                if (rule_package.getDependsOn() != null) {
                    CompositeVersions reqVersions = readPackageList(rule_package.getDependsOnName(), packages);
                    EngineVersion reqValidationVersion = reqVersions.getValidationVersion();
                    if (reqValidationVersion != null && reqValidationVersion.isHigher(versions.getValidationVersion())) {
                        versions.setValidationVersion(reqValidationVersion);
                    }
                    EngineVersion reqEvaluationVersion = reqVersions.getEvaluationVersion();
                    if (reqEvaluationVersion != null && reqEvaluationVersion.isHigher(versions.getEvaluationVersion())) {
                        versions.setEvaluationVersion(reqEvaluationVersion);
                    }
                }
            }
        }
        catch (Exception e) {
            throw new ValidationException("Could not load '" + packageName + "' package", e);
        }
        return versions;
    }
}
