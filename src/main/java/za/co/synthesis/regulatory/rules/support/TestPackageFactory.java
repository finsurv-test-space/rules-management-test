package za.co.synthesis.regulatory.rules.support;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.core.io.Resource;
import za.co.synthesis.rule.core.ValidationException;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

public class TestPackageFactory implements ApplicationContextAware {
  private PackageFactory packageFactory;
  private ApplicationContext appCtx;

  @Override
  public void setApplicationContext(ApplicationContext appCtx) throws BeansException {
    this.appCtx = appCtx;
  }

  public void setPackageFactory(PackageFactory packageFactory) {
    this.packageFactory = packageFactory;
  }

  public String getPackageTest(String packageName, final String name) throws Exception {
    PackageFactory.PackagePack packagePack;
    try {
      packagePack = packageFactory.getPackagePack(packageName);
    } catch (Exception e) {
      throw new ValidationException("Error getting PackagePack", e);
    }
    return loadTestsFromPackagePack(packagePack, name);
  }

  public String convert(InputStream inputStream, Charset charset) throws IOException {

    StringBuilder stringBuilder = new StringBuilder();
    String line = null;

    try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, charset))) {
      while ((line = bufferedReader.readLine()) != null) {
        stringBuilder.append(line).append("\n");
      }
    }

    return stringBuilder.toString();
  }

  private String loadRulesetsFromResource(final String resourceName) throws Exception {
    try {
      Resource resource = appCtx.getResource(resourceName);
      if (resource.exists()) {
        return convert(resource.getInputStream(), StandardCharsets.UTF_8);
      }
    }
    catch (Exception e) {
      throw new Exception("Could not read '" + resourceName + "' file", e);
    }
    throw new Exception("Could not find '" + resourceName + "' file");
  }

  private String loadRulesetsFromPath(final String packagepath, final String name) throws Exception {
    String testFilenamePath = packagepath + File.separator + "tests" + File.separator + name + ".js";
    return loadRulesetsFromResource(testFilenamePath);
  }

  /**
   * This will load rules from a package path.
   * @param packagePack is the package pack to load the rules for
   * @param name is the file that includes the tests
   * @return the engine version required to execute this package of rules
   * @throws Exception
   */
  public String loadTestsFromPackagePack(final PackageFactory.PackagePack packagePack,
                                       final String name) throws Exception {
    PackageFactory.CompositePackage pack = packagePack.getPackage();
    if (pack != null) {
      return loadRulesetsFromPath(pack.getPath(), name);
    }
    throw new Exception("Unexpected scenario where specified package does not exist");
  }
}
