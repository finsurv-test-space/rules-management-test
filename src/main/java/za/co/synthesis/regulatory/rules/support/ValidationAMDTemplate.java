package za.co.synthesis.regulatory.rules.support;

import za.co.synthesis.javascript.JSObject;
import za.co.synthesis.javascript.JSWriter;
import za.co.synthesis.rule.core.EngineVersion;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * User: jake
 * Date: 5/6/16
 * Time: 8:15 AM
 * Given a set of validation rulesets compose a JavaScript file using the AMD template
 */
public class ValidationAMDTemplate implements IValidationTemplate {

  private StringBuilder composedValidationRules(EngineVersion version, Map<String, String> mapping, List<JSObject> rulesetObjs, String comment) {
    List<String> varNames = new ArrayList<String>();

    for (int v=0; v<rulesetObjs.size(); v++) {
      varNames.add("v"+ (v+1));
    }

    StringBuilder sb = new StringBuilder();
    sb.append("define(function () {\n return function(predef) {\n");
    for (String varName : varNames) {
      sb.append("  var ").append(varName).append(";\n");
    }
    sb.append("  with (predef) {").append("\n");

    for (int i=0; i<rulesetObjs.size(); i++) {
      sb.append("    ").append(varNames.get(i)).append(" = ");
      JSWriter writer = new JSWriter();
      writer.setLinePrefix("    ");
      writer.setNewline("\n");
      writer.setIndent("  ");
      rulesetObjs.get(i).compose(writer);
      sb.append(writer.toString());
      sb.append(";\n");
    }
    sb.append("  ").append("}\n");

    sb.append("  return {\n");
    sb.append("    comment: '").append(comment).append("',\n");
    if (version != null) {
      sb.append("    engine: {major: '").append(version.getMajor()).append("', minor: '").append(version.getMinor()).append("'},\n");
    }
    else {
      sb.append("    engine: {major: 'unknown'},\n");
    }
    sb.append("    rules: [");
    boolean bFirst = true;
    for (String varName : varNames) {
      if (bFirst) {
        bFirst = false;
      }
      else {
        sb.append(", ");
      }
      sb.append(varName);
    }
    sb.append("]");

    if (mapping != null && mapping.size() > 0) {
      sb.append(",\n");
      sb.append("    mappings: {");
      bFirst = true;
      for (Map.Entry<String, String> entry : mapping.entrySet()) {
        if (bFirst) {
          bFirst = false;
        }
        else {
          sb.append(", ");
        }
        sb.append(entry.getKey()).append(": \"").append(entry.getValue()).append("\"");
      }
      sb.append("}");
    }
    sb.append("\n");
    sb.append("  };\n");
    sb.append(" };\n");
    sb.append("})\n");

    return sb;
  }

  @Override
  public String compose(EngineVersion version, Map<String, String> mapping, List<JSObject> rulesetObjs, String comment) {
    return composedValidationRules(version, mapping, rulesetObjs, comment).toString();
  }
}
