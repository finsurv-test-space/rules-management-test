package za.co.synthesis.regulatory.rules.support;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.core.io.Resource;
import za.co.synthesis.javascript.JSObject;
import za.co.synthesis.javascript.JSStructureParser;
import za.co.synthesis.javascript.JSUtil;
import za.co.synthesis.rule.core.ValidationException;

import java.io.File;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * User: jake
 * Date: 5/6/16
 * Time: 8:09 AM
 * Provides access to the composed set of validation rules for a given package using a specific template
 */
public class ValidationPackageFactory implements ApplicationContextAware {
  public static enum Location {
    Validation,
    Document
  }

  private static final Map<String, IValidationTemplate> templateMap = new HashMap<String, IValidationTemplate>();

  static {
    registerTemplate("AMD", new ValidationAMDTemplate());
  }

  public static synchronized void registerTemplate(String templateName, IValidationTemplate template) {
     templateMap.put(templateName, template);
  }

  private PackageFactory packageFactory;
  private ApplicationContext appCtx;

  @Override
  public void setApplicationContext(ApplicationContext appCtx) throws BeansException {
    this.appCtx = appCtx;
  }

  public void setPackageFactory(PackageFactory packageFactory) {
    this.packageFactory = packageFactory;
  }

  public String getPackage(String packageName, final Location location, String template) throws ValidationException {
    if (templateMap.containsKey(template)) {
      return composePackage(packageName, location, templateMap.get(template));
    }
    else
      throw new ValidationException("No validation template called '" + template + "' has been registered");
  }

  private String composePackage(String packageName, Location location, IValidationTemplate template) throws ValidationException {
    PackageFactory.PackagePack packagePack;
    try {
      packagePack = packageFactory.getPackagePack(packageName);
    } catch (Exception e) {
      throw new ValidationException("Error getting PackagePack", e);
    }
    List<JSObject> rulesetObjs = new ArrayList<JSObject>();
    loadRulsetsFromPackagePack(packagePack, location, rulesetObjs);

    return template.compose(packagePack.getVersions().getValidationVersion(),
            packagePack.getMapping(), rulesetObjs, packagePack.getValidationPackagePath());
  }

  private void loadRulesetsFromResource(final String resourceName, List<JSObject> rulesetObjs) throws ValidationException {
    try {
      Resource resource = appCtx.getResource(resourceName);
      if (resource.exists()) {
        InputStreamReader reader = new InputStreamReader(resource.getInputStream());
        JSStructureParser parser = new JSStructureParser(reader);
        Object obj = parser.parse();
        JSUtil.findObjectsWith(obj, "ruleset", rulesetObjs);
      }
    }
    catch (Exception e) {
      throw new ValidationException("Could not parse '" + resourceName + "' rule file", e);
    }
  }

  private void loadRulesetsFromPath(final String packagepath, final Location location,
                                    List<JSObject> rulesetObjs) throws ValidationException {
    String validationPath = packagepath + File.separator + (location == Location.Validation ? "validation" : "document");

    String nameTransaction = validationPath + File.separator + "transaction.js";
    loadRulesetsFromResource(nameTransaction, rulesetObjs);

    String nameMoney = validationPath + File.separator + "money.js";
    loadRulesetsFromResource(nameMoney, rulesetObjs);

    String nameImpExp = validationPath + File.separator + "importexport.js";
    loadRulesetsFromResource(nameImpExp, rulesetObjs);
  }

  /**
   * This will load rules from a package path.
   * @param packagePack is the package pack to load the rules for
   * @param location is the location to load the rules from (eithe Validation or Document)
   * @param rulesetObjs the returned list of rulesets
   * @return the engine version required to execute this package of rules
   * @throws Exception
   */
  public void loadRulsetsFromPackagePack(final PackageFactory.PackagePack packagePack,
                                         final Location location,
                                         List<JSObject> rulesetObjs) throws ValidationException {
    PackageFactory.CompositePackage pack = packagePack.getPackage();
    while (pack != null) {
      loadRulesetsFromPath(pack.getPath(), location, rulesetObjs);

      for (PackageFactory.CompositeFeature feature : pack.getFeaturePackages()) {
        loadRulesetsFromPath(feature.getPath(), location, rulesetObjs);
      }
      pack = pack.getParent();
    }
  }
}
