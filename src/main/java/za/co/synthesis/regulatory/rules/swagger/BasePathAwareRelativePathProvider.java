package za.co.synthesis.regulatory.rules.swagger;

import springfox.documentation.spring.web.paths.AbstractPathProvider;

/**
 * Created by jake on 3/31/16.
 */
public class BasePathAwareRelativePathProvider extends AbstractPathProvider {
  private String basePath;

  public BasePathAwareRelativePathProvider(String basePath) {
    this.basePath = basePath;
  }

  @Override
  protected String applicationPath() {
    return basePath;
  }

  @Override
  protected String getDocumentationPath() {
    return "/";
  }
}
