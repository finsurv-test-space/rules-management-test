package za.co.synthesis.regulatory.rules.swagger;

import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.context.request.WebRequest;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.configuration.ObjectMapperConfigured;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.VendorExtension;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;

import static com.google.common.base.Predicates.or;

/**
 * Created by jake on 3/31/16.
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig implements ApplicationListener<ObjectMapperConfigured>
{
   @Bean
    public Docket api() {

        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.any())
                .paths(or(
                        PathSelectors.regex("/evaluation/.*"),
                        PathSelectors.regex("/validation/.*"),
                        PathSelectors.regex("/validate/.*"),
                        PathSelectors.regex("/rules/.*")
                ))
                .build()
                .apiInfo(apiInfo())
                .pathProvider(new BasePathAwareRelativePathProvider("/rules-management"))
                .ignoredParameterTypes(WebRequest.class);
    }



    private ApiInfo apiInfo() {
        ApiInfo apiInfo = new ApiInfo(
                "tXstream Report Data REST API",
                "The tXstream Report Data REST API provides access to all rules data and related functionality",
                "3.0",
                "API Terms Of Service URL",
                new Contact("Synthesis", "http://www.synthesis.co.za", "admin@synthesis.co.za"),
                "API License",
                "API License URL",
                new ArrayList<VendorExtension>()
        );
        return apiInfo;
    }

    @Override
    public void onApplicationEvent(ObjectMapperConfigured objectMapperConfigured) {
        ObjectMapper objectMapper = objectMapperConfigured.getObjectMapper();

        SimpleModule module = new SimpleModule("MyModule", new Version(1, 0, 0, null, null, null));
        module.addSerializer(new SarbAccountTypeSerializer()); // assuming serializer declares correct class to bind to
        objectMapper.registerModule(module);
    }
}
